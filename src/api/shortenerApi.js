 import axios from 'axios';
 import {getEnvVariables} from "../helpers";

const { VITE_API_URL } = getEnvVariables()

const shortenerApi = axios.create({
    baseURL: VITE_API_URL
});

shortenerApi.interceptors.request.use( config => {

    config.headers = {
        ...config.headers,
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
        'Content-Security-Policy': 'upgrade-insecure-requests'
    }

    return config;
})


export default shortenerApi;
