import {useForm, useShortenerStore} from "../../hooks/index.js";
import {useEffect} from "react";
import Swal from "sweetalert2";

const formFields = {
    originalUrl : ''
}

const CreateUrlPage = () => {

    const {originalUrl, onInputChange} = useForm(formFields)

    const {error, startSavingUrl} = useShortenerStore();

    const handleCreate = () => {
        if (originalUrl === null || originalUrl === '') return
        startSavingUrl(originalUrl)
        //navigate('/')
    }

    useEffect(() => {
        if ( error !== undefined ) {
            Swal.fire('Error', error, 'error');
        }
    }, [error])

    return (
        <>
            <h1>Create new URL</h1>

            <div className="mt-5 from-group">
                <label htmlFor="original_url">Original URL</label>
                <input className="form-control" id="original_url" name="originalUrl" value={originalUrl} onChange={onInputChange} placeholder="..." required/>
            </div>

            <div className="d-flex justify-content-end mt-2">
                <button className="btn btn-sm btn-outline-success" onClick={handleCreate}>Create</button>
            </div>
        </>
    );
};

export default CreateUrlPage;
