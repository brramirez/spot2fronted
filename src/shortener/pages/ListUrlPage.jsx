import {useNavigate} from "react-router-dom";
import {CornerUpRight, Plus, Trash} from "react-feather";
import {useShortenerStore} from "../../hooks/index.js";
import {useEffect} from "react";
import Swal from "sweetalert2";


const ListUrlPage = () => {

    const {urls, isLoadingUrls, currentPage, lastPage, startLoadNextUrls, startPreviousPage, startDeletingUrl} = useShortenerStore();
    const navigate = useNavigate();

    const onCreate = () => {
        navigate('/create');
    }

    const handleRedirect = (code ) => {
        navigate(`/${code}`)
    }

    const handleDelete = (code) => {
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: true,
            customClass : {
                confirmButton: "btn btn-success",
                cancelButton: "btn btn-danger me-1"
            },
            buttonsStyling: false
        }).then((result) => {
            if (result.isConfirmed) {
                startDeletingUrl(code)
            }
        });
    }

    useEffect(() => {
        startLoadNextUrls()
    }, []);

    return (
        <>
            <div className="d-grid gap-2 d-flex justify-content-between">
                <h1 className="align-self-start">Shortened URLS:</h1>
                <button className="btn btn-outline-success btn-sm align-self-end" onClick={() => onCreate()}>
                    <Plus/>
                </button>
            </div>

            <table className="table table-hover mt-5">
                <thead>
                <tr>
                    <th>id</th>
                    <th>code</th>
                    <th>original url</th>
                    <th className="text-center">actions</th>
                </tr>
                </thead>
                <tbody>
                {
                    urls.map((url, index) => (
                        <tr key={index}>
                            <td>{url.id}</td>
                            <td>{url.shortened_url}</td>
                            <td>{url.original_url}</td>
                            <td className="text-center">
                                    <button type="button" title="Redirect" className="btn btn-outline-info btn-sm me-2" onClick={() => handleRedirect(url.shortened_url)}>
                                        <CornerUpRight/>
                                    </button>
                                    <button type="button" title="Delete" className="btn btn-outline-danger btn-sm" onClick={() => handleDelete(url.shortened_url)}>
                                        <Trash/>
                                    </button>
                            </td>
                        </tr>
                    ))
                }
                {
                    urls.length === 0 && !isLoadingUrls &&
                    <tr>
                       <td colSpan={4} className="text-center">
                           There are no URLs created
                       </td>
                    </tr>
                }
                </tbody>
            </table>

            <nav aria-label="Page navigation">
                <ul className="pagination">
                    {
                        currentPage != 1 && currentPage < lastPage &&
                        <li className="page-item"><a className="page-link" href="#"
                                onClick={startPreviousPage}>Previous</a>
                        </li>
                    }
                    {
                        !(currentPage >= lastPage) &&
                        <li className="page-item">
                            <a className="page-link" href="#"
                                 onClick={startLoadNextUrls}>Next</a>
                        </li>
                    }
                </ul>
            </nav>
        </>
    );
};

export default ListUrlPage;
