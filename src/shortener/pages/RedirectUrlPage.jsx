import {useParams} from "react-router-dom";
import {useShortenerStore} from "../../hooks/index.js";
import {useEffect} from "react";

const RedirectUrlPage = () => {

    const { code } = useParams();
    const {startSearchUrl} = useShortenerStore()

    useEffect(() => {
        setTimeout(() => {
            startSearchUrl(code)
        }, 1000)
    }, []);

    return (
        <div>
            <h3>Wait a moment...</h3>
        </div>
    );
};

export default RedirectUrlPage;
