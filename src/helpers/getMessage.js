export const getMessage = (e, defaultMessage = '') => {
    const errors = e.response.data.errors
    let message = ''
    Object.keys(errors).forEach(key => {
        errors[key].forEach((msg) => {
            message += `${msg}\n`
        })
    });

    return message || defaultMessage
}
