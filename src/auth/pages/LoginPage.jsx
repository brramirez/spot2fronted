import {useAuthStore, useForm} from "../../hooks/index.js";
import Swal from "sweetalert2";
import {useEffect} from "react";

const formFields = {
    email: '',
    password: ''
}

const LoginPage = () => {

    const {error, startLogin} = useAuthStore()
    const {email, password, onInputChange} = useForm(formFields)

    const handleSubmit = (event) => {
        event.preventDefault();
        startLogin({email, password});
    }

    useEffect(() => {
        if ( error !== undefined ) {
            Swal.fire('Error', error, 'error');
        }
    }, [error])

    return (
        <div className="container">
            <h1>Login</h1>
            <div className="row justify-content-center mt-5">
                <div className="col-6 card p-3">
                    <form onSubmit={handleSubmit}>
                        <div className="form-group">
                            <label htmlFor="email">Email address</label>
                            <input type="email" className="form-control" id="email"
                                   name="email"
                                   value={email}
                                   onChange={onInputChange}
                                   aria-describedby="email" required
                                   placeholder="Enter email"/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input type="password" className="form-control" id="password"
                                   name="password"
                                   value={password}
                                   onChange={onInputChange}
                                   placeholder="Password" required/>
                        </div>

                        <button type="submit" className="btn btn-primary mt-3">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default LoginPage;
