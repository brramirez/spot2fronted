import {createSlice} from "@reduxjs/toolkit";

export const shortenerSlice = createSlice({
    name: 'shortener',
    initialState: {
        isLoadingUrls: true,
        urls: [],
        error: undefined,
    },
    reducers: {
        onLoadUrls: (state, {payload = []}) => {
            state.isLoadingUrls = false;
            state.urls = payload
        },
        onDeleteUrl: (state, {payload}) => {
            state.urls = state.urls.filter(url => url.shortened_url != payload)
        },
        onUrlError: (state, {payload}) => {
            state.error = payload
        },
        onLogoutShortener: (state) => {
            state.isLoadingUrls = true
            state.urls = []
        }
    },
})

// Action creators are generated for each case reducer function
export const {onLoadUrls,onLogoutShortener,onUrlError,onDeleteUrl} = shortenerSlice.actions
