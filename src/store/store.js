import {configureStore} from "@reduxjs/toolkit";
import {authSlice, shortenerSlice} from "./";

export const store = configureStore({
    reducer : {
        auth: authSlice.reducer,
        shortener: shortenerSlice.reducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
        serializableCheck: false
    })
})
