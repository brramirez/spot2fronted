import {createBrowserRouter, createRoutesFromElements, Navigate, Outlet, Route, RouterProvider} from "react-router-dom";
import {CreateUrlPage, ListUrlPage, RedirectUrlPage} from "../shortener";
import {useAuthStore} from "../hooks";
import {LoginPage} from "../auth";
import {useEffect} from "react";

const Root = () => {
    return (
        <div className="container mt-5">
            <Outlet/>
        </div>
    )
}


const router = createBrowserRouter(
    createRoutesFromElements(
        <>
            <Route path="/" element={<Root/>}>
                <Route path="/" element={<ListUrlPage/>}/>
                <Route path="/create" element={<CreateUrlPage/>}/>
                <Route path="/:code" element={<RedirectUrlPage/>}/>
            </Route>
            <Route path="/login" element={<Navigate to="/"/>}/>
        </>
    )
);

const guest = createBrowserRouter(
    createRoutesFromElements(
        <>
            <Route path="/login" element={<LoginPage/>}/>
            <Route path="/*" element={<Navigate to="/login"/>}/>
        </>
    )
);

const AppRouter = () => {
    const {status, checkAuthToken} = useAuthStore();

    useEffect(() => {
        checkAuthToken()
    }, []);

    return <RouterProvider router={status === "authenticated" ? router : guest}/>
};

export default AppRouter;
