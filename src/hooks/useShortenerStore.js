import {useDispatch, useSelector} from "react-redux";
import {onDeleteUrl, onLoadUrls, onLogout, onUrlError} from "../store";
import {shortenerApi} from "../api/index.js";
import {useState} from "react";
import {AxiosError} from "axios";
import {getMessage} from "../helpers/index.js";
import {useNavigate} from "react-router-dom";
import Swal from "sweetalert2";

export const useShortenerStore = () => {

    const [currentPage, setCurrentPage] = useState(0);
    const [lastPage, setLastPage] = useState(0);
    const {isLoadingUrls, urls, error} = useSelector(state => state.shortener);
    const dispatch = useDispatch();

    const navigate = useNavigate();

    const startPreviousPage = () => {
        startLoadUrls(currentPage - 1)
    }

    const startLoadNextUrls = () => {
        startLoadUrls(currentPage + 1)
    }

    const startLoadUrls = async (page = 1) => {
        try{
            const {data} = await shortenerApi.get("/urls",{
                params: {page}
            });
            setCurrentPage(data.current_page)
            setLastPage(data.last_page)
            dispatch(onLoadUrls(data.data));
        }catch(e){
            dispatch(onLogout())
        }
    }

    const startSavingUrl = async (original_url) => {
      try {
          await shortenerApi.post("/urls",{original_url})
          Swal.fire("Success", "URL created correctly", "success")
          navigate("/")
      }catch(e){
          if (e instanceof AxiosError) {
              dispatch(onUrlError(getMessage(e, "Failed to create shortener url")))
          }
      }
    }

    const startDeletingUrl = async (code) => {
      try {
          await shortenerApi.delete(`/urls/${code}`)
          dispatch(onDeleteUrl(code))
      }catch(e){
          if (e instanceof AxiosError) {
              dispatch(onUrlError(getMessage(e, "Failed to delete shortener url")))
          }
      }
    }

    const startSearchUrl = async (code) => {
      try {
          const {data} = await shortenerApi.get(`/urls/${code}`)
          window.location.href = data.original_url;
      }catch(e){
          if (e instanceof AxiosError) {
              dispatch(onUrlError(getMessage(e, "Url not found")))
          }
      }
    }

    return {
        isLoadingUrls,
        urls,
        error,
        currentPage,
        lastPage,
        startLoadNextUrls,
        startSearchUrl,
        startSavingUrl,
        startDeletingUrl,
        startPreviousPage
    }
}
