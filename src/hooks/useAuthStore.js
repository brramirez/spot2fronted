import {useDispatch, useSelector} from "react-redux";
import {shortenerApi} from "../api";
import {clearErrorMessage, onChecking, onLogin, onLogout, onLogoutShortener} from "../store";
import {AxiosError} from "axios";
import {getMessage} from "../helpers/index.js";

export const useAuthStore = () => {

    const {status, user, error} = useSelector(state => state.auth);
    const dispatch = useDispatch();

    const startLogin = async ({email, password}) => {
        dispatch(onChecking())
        try {
            const {data} = await shortenerApi.post('/login', {email, password})
            dispatch(onLogin({user: data}))
            localStorage.setItem('token', data.access_token );
        } catch (e) {
            if (e instanceof AxiosError && e.response) {
                dispatch( onLogout( getMessage(e,'Incorrect credentials' )) );
                setTimeout(() => {
                    dispatch( clearErrorMessage() );
                }, 10);
            }
        }
    }

    const startLogout = () => {
        localStorage.clear();
        dispatch( onLogoutShortener() );
        dispatch( onLogout() );
    }

    const checkAuthToken = () => {
        const token = localStorage.getItem('token');
        if ( !token ) return dispatch( onLogout() );
        dispatch(onLogin({ user: {access_token: token} }));
    }

    return {
        status,
        user,
        error,
        startLogin,
        startLogout,
        checkAuthToken
    }
}
