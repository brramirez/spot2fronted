# URL Shortener

## Development steps

1. Run yarn install
2. Rename the file .env.example to .env
3. Make the respective changes to the environment variables.

```
VITE_API_URL=http://localhost:4000/api

```
