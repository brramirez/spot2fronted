export const initialState = {
    status: 'checking',
    user: {},
    error: undefined,
}

export const authenticatedState = {
    status: 'authenticated',
    user: {
        access_token: 'accessToken',
    },
    error: undefined,
}

export const notAuthenticatedState = {
    status: 'not-authenticated',
    user: {},
    error: undefined,
}

export const checkingState = {
    status: 'checking',
    user: {},
    error: undefined,
}
