import {act, renderHook} from "@testing-library/react";
import {useAuthStore} from "../../src/hooks";
import {configureStore} from "@reduxjs/toolkit";
import {authSlice} from "../../src/store/index.js";
import {initialState, notAuthenticatedState} from "../fixtures/authStates.js";
import {Provider} from "react-redux";
import {testUserCredentials} from "../fixtures/testUser.js";

const getMockStore = (initialState) => {
    return configureStore({
        reducer : {
            auth: authSlice.reducer,
        },
        preloadedState: {
            auth: {...initialState}
        }
    })
}

describe('Test on useAuthStore', () => {
    test('Should return default values', () => {
        const mockStore = getMockStore(initialState)
        const { result } = renderHook(() => useAuthStore(),{
            wrapper: ({ children }) => <Provider store={mockStore}>{children}</Provider>
        });

        expect(result.current).toEqual({
               "error": undefined,
               "checkAuthToken": expect.any(Function),
               "startLogin": expect.any(Function),
               "startLogout": expect.any(Function),
               "status": "checking",
               "user": {},
        })
    })

    test('Should startLogin execute successful', async () => {
        localStorage.clear()

        const mockStore = getMockStore(notAuthenticatedState)
        const { result } = renderHook(() => useAuthStore(),{
            wrapper: ({ children }) => <Provider store={mockStore}>{children}</Provider>
        });

        await act(async () => {
            await result.current.startLogin(testUserCredentials)
        });

        const {error, status, user} = result.current;

        expect({error, status, user}).toEqual({
            error: undefined,
            status: 'notAuthenticated',
            user: expect.any(Object),
        });

        expect(localStorage.getItem("token")).toEqual(expect.any(String));
    })
})
