import shortenerApi from "../../src/api/shortenerApi.js";

describe('Test on shortenerApi', () => {
    test('Should shortenerApi content baseUrl from .env', () => {
        expect(shortenerApi.defaults.baseURL).toBe(process.env.VITE_API_URL);
    })

    test('Should send Authorization on ever requests', async () => {
        const token = "Test-token-example"

        localStorage.setItem('token', token);

        try{
            const {config} = await shortenerApi.get('/shortener')

            expect(config.headers['Authorization']).toBe("Bearer " + token)
        }catch(e){}
    })
})
