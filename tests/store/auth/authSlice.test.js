import {authSlice, clearErrorMessage, onChecking, onLogin, onLogout} from "../../../src/store/index.js";
import {authenticatedState, checkingState, initialState, notAuthenticatedState} from "../../fixtures/authStates.js";
import {testUserCredentials} from "../../fixtures/testUser.js";

describe('authSlice test', () => {
    test('should return the initial state', () => {
        expect(authSlice.getInitialState()).toEqual(initialState)
    })

    test('should do login', () => {

        const state = authSlice.reducer(initialState, onLogin({user : testUserCredentials}))

        expect(state).toEqual({
            ...authenticatedState,
            user: testUserCredentials
        })
    })

    test('should do logout', () => {

        const state = authSlice.reducer(authenticatedState, onLogout())

        expect(state).toEqual(notAuthenticatedState)
    })

    test('should do logout with Error', () => {

        const errorMessage = "Invalid Credentials"

        const state = authSlice.reducer(authenticatedState, onLogout(errorMessage))

        expect(state).toEqual({
            ...notAuthenticatedState,
            error : errorMessage
        })
    })

    test('should be clean error', () => {

        const errorMessage = "Invalid Credentials"

        const state = authSlice.reducer(authenticatedState, onLogout(errorMessage))
        const newState = authSlice.reducer(state, clearErrorMessage())

        expect(newState).toEqual(notAuthenticatedState)
    })

    test('should be start checking', () => {

        const state = authSlice.reducer(notAuthenticatedState, onChecking())

        expect(state).toEqual(checkingState)
    })
})
